# Sample Dockerfile to build a custom Jenkins slave image to be used
# with CERN Jenkins instances (cf. http://cern.ch/jenkinsdocs)

# Start from the base SLC6 or CC7 slave images
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
# Use tag 'slc6' instead of 'cc7' for SLC6
# The FROM statement can be overriden in the GitLab-CI build

# install custom packages (in this example, ghostscript and ghostscript-devel)
### TODO: set the list of packages as necessary
RUN yum install -y \
    zlib.x86_64 \
    zlib-devel.x86_64 \
    expat-devel.x86_64 \
    mesa-libGLU-devel.x86_64 \
    libSM-devel.x86_64 \
    libXmu-devel.x86_64 \
    freeglut-devel.x86_64 \
    mesa-libGL-devel.x86_64 \
    make.x86_64 && \
    yum clean all

